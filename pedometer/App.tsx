import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Value from './src/components/Value';
import RingProgress from './src/components/RingProgress';


const STEPS_GOAL = 10_000;

export default function App() {
  
  const steps: number = 5000;
  const distance: number = steps * 0.68; // Environ

  return (
    <View style={styles.container}>
      <RingProgress radius={100} progress={steps / STEPS_GOAL} />

      <View style={styles.values}>
        <Value label='Pas' value={steps.toString()} />
        <Value label='Distance' value={`${(distance / 1000).toFixed(2).toString()} km`} />
      </View>

      <StatusBar style='auto' />


    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    justifyContent: 'center',
    padding: 12
  },
  values: {
    flexDirection: 'row', 
    gap: 25,
    flexWrap: 'wrap',
    marginTop: 100,
  },
});
