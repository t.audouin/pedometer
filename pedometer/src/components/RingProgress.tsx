import { View, Text } from 'react-native'
import { useEffect } from 'react'
import SVG, { Circle, CircleProps } from 'react-native-svg';
import Animated, { useAnimatedProps, useSharedValue, withSpring, withTiming } from 'react-native-reanimated';

const AnimatedCircle = Animated.createAnimatedComponent(Circle);

type RingProgressProps = {
    radius?: number;
    strokeWidth?: number;
    progress: number;
}

const color = 'red';

const RingProgress = ({
    radius = 100, 
    strokeWidth = 20,
    progress,  
}: RingProgressProps) => {

    const innerRadius = radius - strokeWidth / 2;
    const circumference = 2 * Math.PI * innerRadius;

    const fill = useSharedValue(0);
        fill.value = withTiming(progress, { duration: 1500 });
    useEffect(() => {

    }, [progress])

    const animatedProps = useAnimatedProps(() => ({
        strokeDasharray: [circumference * fill.value, circumference]
    }));

    const circleDefaultProps: CircleProps  = {
        r: innerRadius,
        cx: radius,
        cy: radius,
        originX: radius,
        originY: radius,
        strokeWidth: strokeWidth,
        stroke: color,
        rotation: -90,
    }

  return (
    <View style={{
        width: radius * 2, 
        height: radius * 2, 
        alignSelf: 'center',
        backgroundColor: 'transparent', 
    }}>
      <SVG>
        {/* Background circle */}
        <Circle {...circleDefaultProps} opacity={0.2} fill={'transparent'} />
        {/* Foreground circle */}
        <AnimatedCircle animatedProps={animatedProps} {...circleDefaultProps} />
      </SVG>
    </View>
  )
}

export default RingProgress