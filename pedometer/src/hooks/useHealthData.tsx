import AppleHealthKit, { 
    HealthInputOptions, 
    HealthKitPermissions,
    HealthUnit,
} from 'react-native-health';
import { useEffect, useState } from 'react';

const permissions: HealthKitPermissions = {
  permissions: {
    read: [
      AppleHealthKit.Constants.Permissions.Steps, 
      AppleHealthKit.Constants.Permissions.DistanceWalkingRunning
    ],
    write: [],
  }
}

const useHealthData = (date: Date) => {
    const [hasPermissions, setHasPermissions] = useState(false);
    const [steps, setSteps] = useState(0);
    const [distance, setDistance] = useState(0);

    useEffect(() => {
        AppleHealthKit.initHealthKit(permissions, (err) => {
        if (err) {
            console.log("Error getting permissions");
            return;
        }
        setHasPermissions(true);

        })
    }, [])

    useEffect(() => {
        if (!hasPermissions) {
        return;
        }

        const options: HealthInputOptions = {
        date: new Date().toISOString(),
        includeManuallyAdded: false,
        unit: HealthUnit.meter,
        }

        AppleHealthKit.getStepCount(options, (err, results) => {
        if (err) {
            console.log("Error getting the steps");
            return;
        }
        setSteps(results.value);
        })

        AppleHealthKit.getDistanceWalkingRunning(options, (err, results) => {
        if (err) {
            console.log("Error getting the distance");
            return;
        }
        setDistance(results.value);
        })

    }, [hasPermissions])

    return {
        steps,
        distance
    }
}

export default useHealthData;